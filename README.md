# Snake

This games uses a reinforcement learning algorithm to play the game. It's implemented with Python, PyTorch and Pygame.

## Videotutorial
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/PJl4iabBEz0/0.jpg)](https://www.youtube.com/watch?v=PJl4iabBEz0)

## Installation

- Install python 3.9 (make sure to check add to path)
- Install Anaconda
- `conda create -n snake_pl_env python=3.10`
- `conda activate snake_pl_env`
- `pip3 install pygame`
- `pip3 install torch torchvision`
- `pip3 install matplotlib ipython`